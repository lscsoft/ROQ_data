# Repository for reduced order quadratures of cbc waveform models for use in parameter estimation

Currently the repository contains bases for the `IMRPhenomPv2` waveform model as
implemented in [lalsuite](https://git.ligo.org/lscsoft/lalsuite)'s lalsimulation package.
In subdirectories bases are provided for a number of different segment lengths.
The construction of these bases is discussed in Smith et al, https://arxiv.org/abs/1604.08253. 
Please consider adding a citation to this paper if you are using the bases in analyses.

## Notes:

With `git lfs` installed, one can avoid downloading the whole repository with (for instance):
```
git lfs clone https://git.ligo.org/lscsoft/ROQ_data --include "**/params.dat,*/4s/**"
```
to only download the `params.dat` files and the `4s` basis.
